const webpack           = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  debug: true,
  entry: './app/entry.jsx',
  devtool: 'source-map',
  output: {
    filename: 'bundle.js',
    path: __dirname + '/build'
  },
  resolve: {
    extensions: [ '', '.js', '.jsx']
  },
  module: {
    loaders: [
      {
        test: /\.js(x)?$/,
        loaders: ['react-hot','babel?presets[]=react,presets[]=es2015'],
        exclude: /node_modules/,
      },
      { test: /\.scss$/, loaders: [ 'style', 'css', 'sass' ] }
    ]
  },
  plugins: [
    new webpack.optimize.OccurenceOrderPlugin(),
    //new webpack.optimize.DedupePlugin(),
    new webpack.HotModuleReplacementPlugin(),
    new HtmlWebpackPlugin({
      showError: true,
      template: 'app/index.html',
      chunksSortMode: 'dependency'
    })
  ],
  devServer: {
    contentBase: './build',
    inline: true,
    hot: true,
    port: 4001,
    progress: true,
    stats: { colors: true }
  }
};
