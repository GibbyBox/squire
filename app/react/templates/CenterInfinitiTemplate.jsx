import React from 'react';

const CenterInfinitiTemplate = ({children}) => (
	<div className="center-infinite-template">
		{children}
	</div>
);

export default CenterInfinitiTemplate;