import React, { Component } from 'react';

class MasterTemplate extends Component {
	render() {
		return (
			<div>
				{ this.props.children }
			</div>
		);
	}
}

export default MasterTemplate;