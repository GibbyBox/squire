import React, { Component }       from 'react';
import { MentionsInput, Mention } from 'react-mentions';
import CenterInfinitiTemplate     from '../../templates/CenterInfinitiTemplate';

class TimelineContainer extends Component {
	constructor(props) {
		super(props);

		this._renderHistory = this._renderHistory.bind(this);
		this._changeNewHistoryContent = this._changeNewHistoryContent.bind(this);
		this._createHistoryObject = this._createHistoryObject.bind(this);
		this.testData = [
			{ id: '1235', type: 'town', display: 'Emain Macha' },
			{ id: '0215', type: 'town', display: 'Dunbarton' },
			{ id: '9851', type: 'town', display: 'Tailteann' },
			{ id: '1545', type: 'npc',  display: 'Gadrien' },
			{ id: '6845', type: 'npc',  display: 'Lymilark' },
			{ id: '4565', type: 'npc',  display: 'Cichol' },
			{ id: '3545', type: 'npc',  display: 'Macha' },
			{ id: '2555', type: 'npc',  display: 'Neamhain' },
			{ id: '1111', type: 'npc',  display: 'Morrighan' },
			{ id: '3210', type: 'npc',  display: 'Nuadha' },
			{ id: '8765', type: 'npc',  display: 'Aton Cimeni' },
			{ id: '4315', type: 'npc',  display: 'Danu' },
			{ id: '7888', type: 'npc',  display: 'Jeamiderark' },
			{ id: '8888', type: 'npc',  display: 'Hymerark' },
			{ id: '9777', type: 'npc',  display: 'Eladha' },
			{ id: '6446', type: 'npc',  display: 'Aengus' },
			{ id: '6644', type: 'npc',  display: 'Manannan mac Lir' },
			{ id: '2218', type: 'npc',  display: 'Cromm Cruaich' },
			{ id: '5448', type: 'npc',  display: 'Adniel' },
			{ id: '1222', type: 'npc',  display: 'Bhafel' },
			{ id: '5554', type: 'npc',  display: 'Crumena' },
			{ id: '6664', type: 'npc',  display: 'Languhiris' },
			{ id: '0004', type: 'npc',  display: 'Legatus' },
		];
		this.state = {
			userInput: '',
			history:   []
		};
	}

	_changeNewHistoryContent(e, newValue, newPlaintextValue, mentions) {
		//console.log(e, newValue, newPlaintextValue, mentions);
		this.setState({ userInput: e.target.value });
	}

	_createHistoryObject() {
		let newHistory = this.state.history.slice(0);
		let userInput = this.state.userInput;
		let event = {
			content: userInput,
			date: null/*new Date()*/,
			tags: []
		}

		let regex = /@(.*?)?\(#(\d*)?\)(\+\d*|-\d*)?/g;
		let matches;
		while ((matches = regex.exec(userInput)) !== null) {
			event.tags.push({
				display: matches[1],
				id: matches[2],
				renown: matches[3]
			});
		}

		newHistory.push(event);

		this.setState({
			userInput: '',
			history:   newHistory
		});
	}

	_renderSuggestion(entry, search, highlightedDisplay, index) {
		let className = '';
		if (this.focusIndex === index) {
			className = 'focused suggestion';
		}
		return (
			<span className={className}>
				@{highlightedDisplay}
				<span className="suggestion-id">{`#${entry.id}`}</span>
			</span>
		);
	}

	_renderHistory() {
		return this.state.history.map((instance, index) => (
			<li key={index}>{instance.content}</li>
		));
	}

	_renderInput() {
		return (
			<div className="input-wrapper">
				<MentionsInput 
					className="mentionsInput"
					value={this.state.userInput} 
					onChange={this._changeNewHistoryContent}
					markup="@__display__(#__id__)">
						<Mention 
							data={this.testData}
							type="name" 
							renderSuggestion={this._renderSuggestion} />
				</MentionsInput>
				<button onClick={this._createHistoryObject}>
					Submit
				</button>
			</div>
		);
		//old
		return (
			<div className="input-wrapper">
				<textarea 
					value={this.state.userInput.content || ''}
					onChange={this._changeNewHistoryContent} />
				<button onClick={this._createHistoryObject}>
					Submit
				</button>
			</div>
		);
	}

	render() {
		return (
			<CenterInfinitiTemplate>
				<div className="timeline-wrapper">
					<ul>{ this._renderHistory() }</ul>
					{ this._renderInput() }
				</div>
			</CenterInfinitiTemplate>
		);
	}
}

export default TimelineContainer;