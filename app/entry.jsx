import React          from 'react';
import ReactDom       from 'react-dom';
//import injectTapEventPlugin from 'react-tap-event-plugin';
import TimeLine       from './react/components/TimeLine/TimelineContainer';
import MasterTemplate from './react/templates/MasterTemplate';
import                     './sass/styles.scss';
//injectTapEventPlugin();

ReactDom.render(
  <MasterTemplate>
  	<TimeLine />
  </MasterTemplate>,
  document.getElementById('dashEntry')
);
